const polka = require('polka');

const twitter_routes = require('./routes/twitter.js')

const port = process.env.PORT || 3000

const app = polka()

app.use('/twitter', twitter_routes)

app.listen(port, err => {
  if (err) throw err
  console.log(`> Running on localhost:${port}`)
});
