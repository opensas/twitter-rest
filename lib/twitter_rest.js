// https://www.npmjs.com/package/twitter

var config = require('../config.json')

var Twitter = require('twitter');

var client = new Twitter(config.twitter)

const user_show = (screen_name = '', short = true) => {
  const shorten = ({
    id, id_str, name, screen_name, location, description,
    url, followers_count, created_at, profile_image_url_https }) => {
    return {
      id, id_str, name, screen_name, location, description,
      url, followers_count, created_at, profile_image_url_https
    }
  }
  return new Promise((resolve, reject) => {
    client.get('users/show', { screen_name: screen_name, include_entities: false }, function (error, result, response) {
      if (error)  reject(error)
      else        resolve(short ? shorten(result) : result)
    });
  })
}

const users_search = (q = '', count = 20, page = 1) => {
  return new Promise((resolve, reject) => {
    client.get('users/search', { q, count, page, include_entities: false }, function (error, result, response) {
      if (error) reject(error)
      else resolve(result)
    });
  })
}

//https://twitter.com/i/web/status/1151322634333884416 (id_str)

const user_timeline = (screen_name = '', short = true, count = 200) => {

  const shorten = ({ created_at, id, id_str, full_text, retweet_count, favorite_count }) => {
    return {
      created_at, id, id_str, url: `https://twitter.com/i/web/status/${id_str}`,
      full_text, retweet_count, favorite_count
    }
  }

  return new Promise((resolve, reject) => {
    client.get('statuses/user_timeline', { screen_name, count, include_rts: false, trim_user: false, tweet_mode: 'extended' }, function (error, result, response) {
      if (error) reject(error)
      else resolve(short ? result.map(shorten) : result)
    });
  })
}

const user_timeline_words = (screen_name = '', wordlen = 4, mincount = 3, count = 200) => {
  return new Promise((resolve, reject) => {
    client.get('statuses/user_timeline', { screen_name, count: 200, include_rts: false, trim_user: false, tweet_mode: 'extended' }, function (error, result, response) {
      if (error) reject(error)
      else {
        const texts = result.map(t => cleanShort(cleanTokens(t.full_text.toLowerCase()), wordlen).trim())
        const palabras = texts.reduce((acum, palabra) => acum.concat(palabra.split(' ')), []).sort()
        const sum = sumWords(palabras)
          .filter(sum => sum.count >= mincount)
          .filter(sum => sum.palabra !== '')
          .slice(0, count)
        resolve(sum)
      }
    });
  })
}

const users_timeline_words = (screen_names = '', wordlen = 4, mincount = 3, count = 200) => {
  const names = screen_names.split(',')
  const promises = names.map(name => user_timeline_words(name, wordlen, mincount, count))

  const merge = (a, b) => {
    const c = JSON.parse(JSON.stringify(a.concat(b))) // deep clone
    const merged = []
    c.forEach(current => {
      const index = merged.findIndex(item => item.palabra === current.palabra)
      if (index === -1) merged.push(current)
      else merged[index].count += current.count
    });
    return merged
  }

  return new Promise((resolve, reject) => {
    Promise.all(promises).then(results => {
      const combined = results.reduce((acum, result) => merge(acum, result), [])
        .sort((a, b) => b.count - a.count)
        .slice(0, count)

      let result = { combined }

      results.forEach((item, index) => {
        result[names[index]] = results[index]
      })
      resolve(result)

    }).catch(error => {
      reject(error)
    })
  });
}

const sumWords = (words) => {
  const sum = []
  if (!words || words.length === 0) return sum

  let current = words[0]
  let count = 0

  for (index = 0; index <= words.length - 1; index++) {
    if (current == words[index]) count++
    else {
      current = current.replace(/anio/g, 'año').replace(/campania/g, 'campaña')
      sum.push({ word: current, count: count })
      current = words[index]
      count = 0
    }
  }
  sum.push({ palabra: current, count: count })
  return sum.sort((a, b) => b.count - a.count)
}

const cleanTokens = (text) => {
  return text
    .replace(/\./g, ' ')
    .replace(/\,/g, ' ')
    .replace(/desde/g, '')
    .replace(/porque/g, '')
    .replace(/en vivo/g, '')
    .replace(/\:/g, '')
    .replace(/á/g, 'a')
    .replace(/é/g, 'e')
    .replace(/í/g, 'i')
    .replace(/ó/g, 'o')
    .replace(/ú/g, 'u')
    .replace(/ñ/g, 'ni')
    .replace(/https.*$/g, '')
    .replace(/\W+/g, ' ')
  // .replace(/https/g, '')
}
const cleanShort = (text, len = 3) => {
  const r = new RegExp(`\\b\\w{1,${len}}\\b`, 'g')
  return text.replace(r, '').replace(/\W+/g, ' ')
}

module.exports = { user_show, users_search, user_timeline, user_timeline_words, users_timeline_words }