const polka = require('polka');

const twitter = require('../lib/twitter_rest.js')

const app = polka()

app.get('/', async (req, res) => {
  const url = req.headers.host + req.originalUrl.replace(/\/$/, '');
  const result = [
    `twitter-rest v1.0.0`,
    `http://${url}/user_show?user=alferdez&short=1`,
    `http://${url}/user_timeline?user=alferdez&count=1&short=1`,
    `http://${url}/user_timeline_words?user=alferdez&wordLen=3&mincount=4&count=5`,
    '---',
    `http://${url}/users_search?q=macri&count=5&page=1`,
    `http://${url}/users_timeline_words?user=alferdez,mauriciomacri,CFKargentina&wordLen=3&mincount=4&count=5`
  ]
  // console.log(result)
  res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
  res.end(JSON.stringify(result, null, 2))
})

app.get('/users_timeline_words', async (req, res) => {
  const q = parseQuery(req, res); if (!q) return

  try {
    const result = await twitter.users_timeline_words(q.user, q.wordlen, q.mincount, q.count)
    // console.log(result)
    res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(result, null, 2))
  } catch (e) {
    res.writeHead(400, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(e, null, 2))
  }
})

app.get('/user_timeline_words', async (req, res) => {
  const q = parseQuery(req, res);
  if (!q) return

  try {
    const result = await twitter.user_timeline_words(q.user, q.wordlen, q.mincount, q.count)
    // console.log(result)
    res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(result, null, 2))
  } catch (e) {
    res.writeHead(400, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(e, null, 2))
  }
})

app.get('/user_timeline', async (req, res) => {
  const q = {
    user: req.query.user,
    short: req.query.short === '0' ? false : true,
    count: parseInt(req.query.count || 200)
  }
  if (!q.user) return (res.statusCode = 400, res.end('no user specified'))

  try {
    const result = await twitter.user_timeline(q.user, q.short, q.count)
    console.log(result)
    res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(result, null, 2))
  } catch (e) {
    res.writeHead(400, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(e, null, 2))
  }
})

app.get('/user_show', async (req, res) => {
  const q = {
    user: req.query.user,
    short: req.query.short === '0' ? false : true,
  }
  if (!q.user) return (res.statusCode = 400, res.end('no user specified'))

  try {
    const result = await twitter.user_show(q.user, q.short)
    console.log(result)
    res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(result, null, 2))
  } catch (e) {
    console.log(e)
    res.writeHead(400, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(e, null, 2))
  }
})

app.get('/users_search', async (req, res) => {
  const q = {
    q: req.query.q || '',
    count: parseInt(req.query.count || 20),
    page: parseInt(req.query.page || 1)
  }
  if (!q.q) return (res.statusCode = 400, res.end('no q specified'))

  try {
    const result = await twitter.users_search(q.q, q.count, q.page)
    // console.log(result)
    res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(result, null, 2))
  } catch (e) {
    res.writeHead(400, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
    res.end(JSON.stringify(e, null, 2))
  }

})

const parseQuery = (req, res) => {
  const user = req.query.user
  if (!user) {
    res.statusCode = 400
    res.end('no user specified')
    return false
  }

  return {
    user,
    wordlen: parseInt(req.query.wordlen || 4),
    mincount: parseInt(req.query.mincount || 3),
    count: parseInt(req.query.count || 200)
  }
}

module.exports = app
